import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  team = 'red';
  status = 'stop';
  rounds = 0;
  interVal: any;
  sendetSpeed = -1;
 constructor(private http: HttpClient) {
 }

 ngOnInit(): void {
   this.interVal = setInterval(() => {
     this.sendSpeed();
   }, 1000);
 }

  sendStatus(status: string) {
   const  body = {rounds: this.rounds };
   this.http.post(environment.API + this.team + '/' + status, body, {headers: this.get_Header()})
     .subscribe(
       data => {
         console.log(data);
         this.rounds = 0;
       },
      error => console.log(error)
  );
 }

 sendSpeed() {

   if (this.status === 'start') {
     const body = {
       speed: Math.floor((Math.random() * 100) + 1)
     };

     this.sendetSpeed = body.speed;
     this.http.post(environment.API + 'status', body, {headers: this.get_Header()})
       .subscribe(
        data => console.log(data),
        error => console.log(error)
       );
   }
 }



  get_Header() {
   return new HttpHeaders({'Content-Type': 'application/json'});
  }
}
